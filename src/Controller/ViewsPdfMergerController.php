<?php

namespace Drupal\views_pdf_merger\Controller;

use Drupal\entity_print_views\Controller\ViewPrintController;
use Drupal\file\Entity\File;
use Drupal\Core\Url;
use Clegginabox\PDFMerger\PDFMerger;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Generates the PDF from the view and merge it with PDF referenced by the view.
 */
class ViewsPdfMergerController extends ViewPrintController {

  /**
   * Redirect to the route to merge PDF and open it in the browser.
   *
   * The goal is to add the view's title to the route to use it as the title
   * of a merged PDF (the last route argument is used as a title).
   *
   * @param string $view_name
   *   The view name.
   * @param string $display_id
   *   The view display to render.
   * @param string $pdf_for_merging_id
   *   The id of the PDF file for merging with the PDF generated from the view.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The RedirectResponse object.
   */
  public function viewPrint($view_name, $display_id, $pdf_for_merging_id) {

    /** @var \Drupal\views\Entity\View $view */
    $view = $this->entityTypeManager->getStorage('view')->load($view_name);

    // Open PDF in the browser.
    $route_parameters = [
      'export_type' => 'pdf',
      'view_name' => $view_name,
      'display_id' => $display_id,
      'pdf_for_merging_id' => $pdf_for_merging_id,
      'view_title' => $view->label(),
    ];

    $options = [
      'query' => $this->currentRequest->query->all(),
    ];

    return $this->redirect('views_pdf_merger.display_merged_pdf_in_browser', $route_parameters, $options);
  }

  /**
   * Merge two PDF files and display the merged file in a browser.
   *
   * @param string $view_name
   *   The view name.
   * @param string $display_id
   *   The view display to render.
   * @param string $pdf_for_merging_id
   *   The id of the PDF file for merging with the PDF generated from the view.
   * @param string $view_title
   *   The view title (for display as PDF title).
   *
   * @return string
   *   PDF document.
   */
  public function displayMergedPdfInBrowser($view_name, $display_id, $pdf_for_merging_id, $view_title) {
    $export_type = 'pdf';

    // Create the Print engine plugin.
    $config = $this->config('entity_print.settings');

    /** @var \Drupal\views\Entity\View $view */
    $view = $this->entityTypeManager->getStorage('view')->load($view_name);
    $executable = $view->getExecutable();
    $executable->setDisplay($display_id);

    if ($args = $this->currentRequest->query->get('view_args')) {
      $executable->setArguments($args);
    }

    try {
      $print_engine = $this->pluginManager->createSelectedInstance($export_type);
    }
    catch (PrintEngineException $e) {
      // Build a safe markup string using Xss::filter() so that the instructions
      // for installing dependencies can contain quotes.
      $this->messenger()->addError(new FormattableMarkup('Error generating Print: ' . Xss::filter($e->getMessage()), []));

      $url = $executable->hasUrl(NULL, $display_id) ? $executable->getUrl(NULL, $display_id)->toString() : Url::fromRoute('<front>');
      return new RedirectResponse($url);
    }

    // The name of the PDF file generated from the view. This file will be
    // deleted after merging with the PDF file referenced by the view's field.
    $generated_pdf_filename = uniqid($view_name, TRUE) . '.pdf';

    $this->printBuilder->savePrintable([$view], $print_engine, $scheme = 'public', $generated_pdf_filename, $config->get('default_css'));

    // Get the URI of the PDF referenced from the view.
    $file = File::load($pdf_for_merging_id);

    $uri_of_pdf_referenced_by_view = $file->getFileUri();
    $uri_of_pdf_generated_from_view = ('public://' . $generated_pdf_filename);

    // Merge two PDF files.
    $pdf_merger = new PDFMerger();
    $pdf_merger->addPDF($uri_of_pdf_generated_from_view, 'all');
    $pdf_merger->addPDF($uri_of_pdf_referenced_by_view, 'all');

    $response = new StreamedResponse();

    $response->setCallback(function () use ($pdf_merger, $view_title, $generated_pdf_filename) {
      $pdf_merger->merge('browser', $view_title . '.pdf');
      // Delete the temporary PDF file generated from the view.
      \Drupal::service('file_system')->delete('public://' . $generated_pdf_filename);
    });

    return $response;
  }

}
